G04 #@! TF.GenerationSoftware,KiCad,Pcbnew,6.0.5*
G04 #@! TF.CreationDate,2022-06-17T17:54:00-04:00*
G04 #@! TF.ProjectId,sit1566-breakout,73697431-3536-4362-9d62-7265616b6f75,rev?*
G04 #@! TF.SameCoordinates,Original*
G04 #@! TF.FileFunction,Soldermask,Top*
G04 #@! TF.FilePolarity,Negative*
%FSLAX46Y46*%
G04 Gerber Fmt 4.6, Leading zero omitted, Abs format (unit mm)*
G04 Created by KiCad (PCBNEW 6.0.5) date 2022-06-17 17:54:00*
%MOMM*%
%LPD*%
G01*
G04 APERTURE LIST*
%ADD10R,1.700000X1.700000*%
%ADD11O,1.700000X1.700000*%
%ADD12C,0.350000*%
%ADD13C,3.200000*%
G04 APERTURE END LIST*
D10*
G04 #@! TO.C,J1*
X144780000Y-106680000D03*
D11*
X147320000Y-106680000D03*
X149860000Y-106680000D03*
G04 #@! TD*
D12*
G04 #@! TO.C,U1*
X147820000Y-101395000D03*
X146820000Y-101395000D03*
X146820000Y-101805000D03*
X147820000Y-101805000D03*
G04 #@! TD*
D13*
G04 #@! TO.C,REF\u002A\u002A*
X147320000Y-97790000D03*
G04 #@! TD*
M02*
